﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDo.Users.Model
{
    public class User
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }
        public DateTime CreateDateTime { get; set; }
        public DateTime ModDateTime { get; set; }
        public int Id { get; set; }

        public User(string name, string address, int age)
        {
            Name = name;
            Address = address;
            Age = age;
            CreateDateTime = DateTime.Now;
        }

        public string Show()
        {
            return Name + " " + Address + " " + Age.ToString() + " Data modyfikacji: " + ModDateTime.ToString("D");
        }

        public string Show(string phoneNumber)
        {
            return Name + " " + Address + " " + Age.ToString() + " " + phoneNumber;
        }

        public void FixName(string newName)
        {
            Name = newName;
            ModDateTime = DateTime.Now;
        }
    }
}
