﻿using System;
using ToDo.Users.Model;

namespace ToDo.Users
{
    class Program
    {
        static void Main(string[] args)
        {
            //var kuba = new User("Jakub Mrowca","Przyszowa",27);

            //Console.WriteLine("Hello World!");

            //Console.WriteLine(kuba.Show());
            //Console.WriteLine(kuba.Show("517123123"));

            //kuba.FixName("Jacek");
            //Console.WriteLine(kuba.Show());

            Console.WriteLine("Podaj imie i nazwisko!");
            var name = Console.ReadLine();

            Console.WriteLine("Podaj adres!");
            var address = Console.ReadLine();

            Console.WriteLine("Podaj wiek!");
            var age = ParseAge();

            var user = new User(name, address, age);
            Console.WriteLine(user.Show());

        }

        public static int ParseAge()
        {
            var age = Console.ReadLine();
            var ageNumber = 0;
            var result = int.TryParse(age, out ageNumber);
            if (result == true)
            {
                return ageNumber;
            }
            else
            {
                Console.WriteLine("Zły wiek. Podaj jeszcze raz!");
                return ParseAge();
            }
        }
    }
}
