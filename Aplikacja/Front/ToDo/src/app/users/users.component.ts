import { Component, OnInit } from '@angular/core';
import { User } from './model/user';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  name: string = "";
  address: string = "";
  age:number = 0;
  users:Array<User> = new Array<User>();
  displayedColumns: string[] = ['Name', 'Address', 'Age'];
  constructor() { }

  ngOnInit(): void {
  }

  add(){
    var user = new User(this.name,this.address,this.age);
    this.users.push(user);
    console.log(user);
    this.clear();
    
  }
  clear(){
    this.name = "";
    this.address = "";
    this.age = 0;
  }
}
